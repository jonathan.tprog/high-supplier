<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    //Refiere a la página de restaurante
    public function openRestaurant(){
        return view('restaurants');
    }
}
