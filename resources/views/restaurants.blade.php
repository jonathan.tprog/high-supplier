@include('header')
@include('modal')

<section class="ftco-section-2" style="background: #79d5c9;">
    <div class="photograhy">
        <div align="center">
            <h3>Datos del comercio</h3>
            <hr>
        </div>
        <form id="formCommerce">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formGroupName">
                            Nombre del comercio
                        </label>

                        

                        <input type="text" class="form-control" id="formControlName" placeholder="Ingresa el nombre" required>
                    </div>

                    <div class="form-group">
                        <label for="formGroupImg">
                            Logo del comercio
                        </label>

                        <div class="input-file input-file--reverse">
                            <input type="text"  style="background: transparent;" id="lblLogo" disabled class="input-file__field"></label>
                            <label class="btn btn-primary">Seleccionar Archivo
                                <input type="file" id="formControlImgInput" onchange="return fileValidationImg()" class="form-control" accept="image/*" hidden>
                            </label>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formGroupAddress">
                            Dirección del comercio
                        </label>
                        <input type="text" class="form-control" id="formControlAddressInput" placeholder="Ingresa la dirección " required>
                    </div>

                    <div class="form-group">
                        <label for="formGroupRFC">
                            RFC con domicilio fiscal
                        </label>
                        <input type="text" class="form-control" id="formControlRFCInput" placeholder="Ingresa el RFC " required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formGroupAddressBusinessName">
                            Razón social
                        </label>
                        <input type="text" class="form-control" id="formControlBusinessNameInput" placeholder="Ingresa la razón social " required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formGroupLicence">
                            Licencia de funcionamiento
                        </label>
                        <div class="input-file input-file--reverse">
                            <input type="text" disabled style="background: transparent;" id="lblLicence" class="input-file__field"></label>
                            <label class="btn btn-primary">Seleccionar Archivo
                                <input type="file" class="form-control" accept="application/pdf" onchange="return fileValidationPDF()" hidden id="formControlLicenseInput" required>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formGroupINE">
                            INE del representante legal
                        </label>
                        <div class="input-file input-file--reverse">
                            <input type="text" disabled style="background: transparent;" id="lblINE" class="input-file__field"></label>
                            <label class="btn btn-primary">Seleccionar Archivo
                                <input type="file" class="form-control" accept="application/pdf" hidden id="formControlINEInput" onchange="return fileValidationPDFINE()" required>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formCtrlEmailInput">
                            Correo
                        </label>
                        <input type="email" class="form-control" id="formControlEmailInput" name="formControlEmailInput" placeholder="Ingresa su correo" onkeyup="validateEmail(this)" required><a id='resp'></a>
                    </div>
                </div>
                <div class="col-sm-6">

                    <div class="form-group">
                        <label for="formGroupTime">
                            Horario Entrada
                        </label>
                        <input type="time" class="form-control" id="formControlOpenTimeInput" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formGroupTime">
                            Horario Salida
                        </label>
                        <input type="time" class="form-control" id="formControlCloseTimeInput" required>
                    </div>
                </div>
                <div class="col-sm-6">

                    <div class="form-group">
                        <label for="formOrderNumber">
                            Número de pedidos
                        </label>
                        <input type="text" onkeypress="return onlyNumber(event)" class="form-control" placeholder="Ingresa el número de pedidos" id="formControlOrderNumeberInput" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formContactNumber">
                            Número de contacto
                        </label>
                        <input type="text" onkeypress="return onlyNumber(event)" class="form-control" id="formControlContactNumeberInput" placeholder="Ingresa el número de contacto" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formEmergencyNumber">
                            Número de emergencia
                        </label>
                        <input type="text" onkeypress="return onlyNumber(event)" class="form-control" id="formControlEmergencyNumeberInput" placeholder="Ingresa el número de emergencia" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-check">
                        <label for="formBill" class="switch">
                            ¿Requiere factura?
                            <br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" value="Si" name="factura" class="form-check-input"><label for="formBill" class="form-check-label">Si</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" value="No" name="factura" class="form-check-input"><label for="formBill" class="form-check-label">No</label>
                        </label>
                    </div>
                </div>
                <div class="col-sm-12">

                    <div class="form-group">
                        <label for="formPosition">
                            Posición de la persona que está dando de alta
                        </label>
                        <input type="text" class="form-control" id="formControlPositionInput" placeholder="Ingresa la posición" required>
                    </div>
                </div>
            </div>
        </form>
        <div align="center">
            <h3>Datos bancarios</h3>
            <hr>
        </div>
        <form>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formBank">
                            Banco
                        </label>
                        <input type="text" class="form-control" id="formControlBankInput" placeholder="Ingresa el banco" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formAccountNumber">
                            Número de cuenta
                        </label>
                        <input type="password" onkeypress="return onlyNumber(event)" class="form-control" placeholder="Ingresa el número de cuenta" id="formControlAccountNumberInput" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formClabe">
                            Clabe
                        </label>
                        <input type="password" onkeypress="return onlyNumber(event)" class="form-control" placeholder="Ingresa la clabe" id="formControlClabeInput" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="formOwner">
                            Nombre del tituar
                        </label>
                        <input type="text" class="form-control" id="formOwnerInput" placeholder="Ingresa el nombre del titular" required>
                    </div>
                </div>
            </div>
        </form>


        <div align="center">
            <h3>Menú</h3>
            <hr>
        </div>
        <form>
            <div class="row">
                <div class="col-sm-12">
                    <div align="center">
                        <div class="form-group">
                            <input type="button" class="btn btn-primary" id="formFoodBtn" onclick="addTable()" value="Insertar Platillo">
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table" id="tableFood">
                        <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Imagenes</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        <div class="col-sm-12">
        <div align="center">
                <div class="form-group">
                    <input type="button" class="btn btn-primary" onclick="abrir()" id="formTyC" value="Términos y condiciones">
                </div>
            <div align="center">
                <div class="form-group">
                    <input type="button" class="btn btn-primary" style="background-color: #f74a21; border-color: #f7a21; border: red; color: white;" disabled="true" onclick="sendInfo()" id="formFinishBtn" value="Finalizar">
                </div>
            </div>
        </div>
    </div>
</section>
@include('footer')