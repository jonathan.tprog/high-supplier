</div><!-- END COL -->
</div>
</div>
</section>

</div><!-- END COLORLIB-MAIN -->
</div><!-- END COLORLIB-PAGE -->
<script src="../resources/assets/js/jquery.min.js"></script>
<script src="../resources/assets/js/jquery.validate.js"></script>
<script src="../resources/assets/js/jquery-migrate-3.0.1.min.js"></script>
<script src="../resources/assets/js/popper.min.js"></script>
<script src="../resources/assets/js/bootstrap.min.js"></script>
<script src="../resources/assets/js/jquery.easing.1.3.js"></script>
<script src="../resources/assets/js/jquery.waypoints.min.js"></script>
<script src="../resources/assets/js/jquery.stellar.min.js"></script>
<script src="../resources/assets/js/owl.carousel.min.js"></script>
<script src="../resources/assets/js/jquery.magnific-popup.min.js"></script>
<script src="../resources/assets/js/aos.js"></script>
<script src="../resources/assets/js/jquery.animateNumber.min.js"></script>
<script src="../resources/assets/js/bootstrap-datepicker.js"></script>
<script src="../resources/assets/js/scrollax.min.js"></script>
<script src="../resources/assets/js/restaurants.js"></script>
<script src="../resources/assets/js/main.js"></script>
</body>
<script>
    $(function() {
        $('.input-file__input').on('change', function() {
            if ($(this)[0].files[0]) {
                $(this).prev().text($(this)[0].files[0].name);
            }
        })
    })
    function abrir(){
	   $('#myModal').modal({
          show:true
        });
    }
</script>
<script type="text/javascript">
      $(document).ready(function(){
        $(document).on('click', '.form-check-input', function(){
            let val = $(this).val();
            if($(this).is(':checked')){
                $("#formFinishBtn").attr("disabled", false);
            }else{
                $("#formFinishBtn").attr("disabled", true);
            }
        })
        
      });
     
  </script>

</html>