  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" data-backdrop="static">
      <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title">Términos y condiciones</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                  <p>
                      <font size="0"><b>CONTRATO DE SERVICIOS QUE CELEBRAN POR UNA PARTE LA SOCIEDAD MERCANTIL DENOMINADA OPERADORA DE APLICACIONES
                              AMÉRICA SA DE CV, REPRESENTADA EN ESTE ACTO POA RICARDO SALDAÑA OBREGÓN, A QUlEN EN LO SUCESIVO SE LE DENOMINARÁ
                              "OPERADORA OE APLICACIONES AMÉRICA" Y POR LA OTRA LA PERSONA GOE SE ESTABLECE EN EL ANVERSO DEL PRESENTE
                              CONTRATO, A QUIEN EN LO SUCESIVO SE LE DENOMINARÁ "EL RESTAURANTE", A QUIENES CONJUNTAMENTE SE LES DENOMINARÁ
                              COMO "LAS PARTES", AL TENOR DE LAS SIGUIENTES DECLARACIONES Y CLÁUSULAS:</b></font>
                  </p>
                  <div class="col-sm-6" style="float: left;">
                      <p>
                          <font size="0">
                          DECLARACIONES
                              Declara "OPERADORA DE APLICACIONES AMÉRICA". por conducto de
                              su representante legal que:Es una sociedad mercantil legalmente consti
                              tuida de conformidad con la legislación mexicana, tal y como se des
                              prende de la Escritura Pública número 31,237 (treinta y un mil doscien
                              tos treinta y siete) de fecha 11 (once) de julio de 2016 (dos mil dieci
                              séis), pasada ante la fe del Lic. Moisés Solís García, Notario Público
                              Adscrito a la Notaría número 33 (treinta y tres) de la ciudad de Santia
                              go de Querétaro, Querétaro, cuyo primer testimonio de encuentra debi
                              damente inscrito en el Registro Público de Comercio de Querétaro.
                              El señor RICARDO SALDAÑA OBREGÓN, cuenta con las facultades sufi
                              cientes para la suscripción del presente contrato, manifestando que
                              dichas facultades no le han sido modificadas o limitadas en forma
                              alguna. Su domicilio está ubicado en Av. Industrialización 12, Álamos
                              Segunda Sección, Querétaro, Querétaro, C.P. 76160. Que tiene contrato
                              de prestación de servicios con la empresa que cuenta con la autoriza
                              ción para el uso de la marca YAPPAPP y quien además administra la
                              aplicación móvil denominada YAPPAPP. Aplicación móvil para teléfonos
                              inteligentes con sistemas operativos Android e iOS, que brinda solucio
                              nes en las áreas de envíos de productos y seguridad para los ciudada
                              nos de la República Mexicana. Dicha aplicación permite a los usuarios
                              que se registren en ella, realizar pedidos a domicilio de diversos servi
                              cios entre los que se encuentran el servicio de REPARTO A DOMICILIO
                              DE PRODUCTOS SUSCEPTIBLES DE ENTREGA. En virtud del contrato
                              de prestación de servicios mencionado en el inciso inmediato anterior,
                              "OPERADORA DE APLICACIONES AMÉRICA" cuenta con las facultades
                              suficientes y necesarias para celebrar contrato de prestación de servi
                              cios con los proveedores encargados de prestar los servicios requeridos
                              por usuarios de la aplicación YAPPAPP única y exclusivamente en la RE
                              PÚBLICA MEXICANA. Que es su voluntad celebrar el presente contrato,
                              encontrándose libre de error, dolo, mala fe, violencia o cualquier otro
                              vicio del consentimiento que pudiera afectar la validez del contrato. Por
                              lo anterior, las partes expresamente se sujetan a las siguientes CLÁUSU
                              LAS: PRIMERA. OBJETO.- las partes reconocen expresamente y aceptan
                              que "OPERADORA DE APLICACIONES AMÉRICA", de conformidad a la
                              declaración I inciso D, cuenta con las facultades necesarias para cele
                              brar y ejecutar el presente contrato en los términos que se especifican,
                              reconociendo, además, que el titular de la aplicación móvil es un pro
                              veedor de servicios de tecnología, en particular de la aplicación móvil
                              denominada YAPPAPP, la cual permite a sus usuarios realizar pedidos a
                              domicilio de diversos servicios, entre los que se encuentra el servicio
                              de COMIDA A DOMICILIO y que "EL RESTAURANTE" es un prestador
                              de servicios de alimentos y que se encuentra actualmente con la posibi
                              lidad de prestar servicio de COMIDA PARA LLEVAR en los términos y
                              condiciones que se pactan en el siguiente contrato. Ambas partes
                              acuerdan celebrar el presente contrato con el objeto de que la aplica
                              ción móvil denominada YAPPAPP sea un medio por el cual se promo
                              cione los servicios de COMIDA PARA LLEVAR que ofrece "EL RESTAU
                              RANTE" y se obliga a atender los pedidos de COMIDA PARA LLEVAR
                              en los términos que más adelante se pactan. SEGUNDA. TERRITORIO,
                              INFORMACIÓN Y REQUISITOS.- acuerdan las partes que los servicios de
                              COMIDA PARA LLEVAR que presta "EL RESTAURANTE" se ofrecerán
                              mediante la aplicación YAPPAPP exclusivamente dentro del territorio de
                              LA ZONA METROPOLITANA DE QUERÉTARO, INCLUYENDO MUNICI
                              PIOS DE QUERÉTARO, CORREGIDORA Y EL MARQUÉS. Lo anterior es
                              para estar en posibilidad de entregar el pedido que realicen los usua
                              rios de la aplicación YAPPAPP en el tiempo ofrecido. "EL RESTAURAN
                              TE" se obliga a contar con un equipo de cómputo, un teléfono inteli
                              gente o una Tablet y servicio de internet, lo que permitirá tener acceso
                              a la aplicación YAPPAPP y poder atender y dar seguimiento a los pedi
                              dos solicitados por los usuarios de la aplicación. De igual forma se
                              obliga a asignar a una persona que atenderá, durante el horario de
                              atención señalada, los pedidos que se generen mediante la aplicación
                              YAPPAPP. TERCERA. ACCESO A LA APLICACIÓN.- "OPERADORA DE
                              APLICACIONES AMÉRICA" dará de alta en la aplicación móvil YAPPAPP
                              a "EL RESTAURANTE" en un plazo de 7 (siete) días hábiles para que
                              pueda comenzar a atender los pedidos que realizarán los usuarios de la
                              aplicación. "OPERADORA DE APLICACIONES AMÉRICA" hace del cono
                              cimiento de "EL PROVEEDOR" y éste lo acepta expresamente, que la
                              aplicación YAPPAPP tendrá dos fases operativas para informar y hacer
                              del conocimiento de "EL RESTAURANTE" la existencia de pedidos por
                              preparar: PRIMERA FASE en la que "OPERADORA DE APLICACIONES
                              AMÉRICA" se obliga a informar y confirmar los pedidos con "EL RES
                              TAURANTE" vía telefónica o mediante mensaje utilizando la aplicación
                              "whatsapp", para garantizar que "EL RESTAURANTE" tenga conocimien
                              to del pedido solicitado e inicie su preparación para entregarlo en el
                              tiempo que corresponda. Esta primera fase entrará en funcionamiento a
                              partir de que "EL RESTAURANTE" haya sido dado de alta en la aplica
                              ción móvil YAPPAPP y no concluirá hasta que entre en funcionamiento
                              la SEGUNDA FASE. Previa notificación mediante correo electrónico con
                              acuse de recibido por parte de "OPERADORA DE APLICACIONES AMÉ
                              RICA" a "EL RESTAURANTE" con cinco días hábiles de anticipación, ini
                              ciará la operación de la segunda fase, en la que "OPERADORA DE
                              APLICACIONES AMÉRICA" se obliga a informar y notificar los pedidos
                              que hayan sido solicitados por usuarios, única y exclusivamente vía
                              electrónica en la aplicación móvil YAPPAPP, para lo cual "EL RESTAU-
                              RANTE" se obliga a revisar de forma constante, las veces que sean ne
                              cesarías, la información que aparece en la aplicación para estar en posi
                              bilidad de surtir los pedidos, y en su caso, nombrar a un encargado que
                              esté al pendiente para la atención oportuna.

                          </font>
                      </p>
                  </div>
                  <div class="col-sm-6" style="float: right;">
                      <p>
                          <font size="0">
                              CUARTA. COBRO Y PAGO
                              DE LOS SERVICIOS.- acuerdan las partes que será OPEN PAY o cual
                              quier otra plataforma de pagos que en su momento informe "OPERA
                              DORA DE APLICACIONES AMÉRICA" quien realice el cobro de la canti
                              dad que determine "EL RESTAURANTE" cobrar por los servicios de
                              COMIDA PARA LLEVAR y por ello podrá usar como métodos de cobro
                              tarjeta de crédito o débito, aceptando tarjetas VISA, MASTERCARD y
                              AMERICAN EXPRESS.
                              Por la utilización de la aplicación móvil YAPPAPP, "OPERADORA DE
                              APLICACIONES AMÉRICA" cobrará a "EL RESTAURANTE" la cantidad
                              que corresponde de aplicar el 19% (diecinueve por ciento) al monto
                              total de cada pedido, otorgado desde este momento, "EL RESTAURAN
                              TE" da autorización para que "OPERADORA DE APLICACIONES AMÉRI
                              CA' o la persona que se designe, retenga del monto cobrado dicha
                              cantidad. Por la prestación de los servicios de COMIDA PARA LLEVAR
                              entregamos en tiempo y forma, "EL RESTAURANTE" tendrá derecho a
                              recibir el 81% (ochenta y un porciento) del monto total de cada pedido.
                              Para ello "OPERADORA DE APLICACIONES AMÉRICA" realizará cortes
                              semanales los días lunes a las 24.00 horas (tiempo del centro) y las
                              cantidades que correspondan a "EL RESTAURANTE" se le entregarán de
                              la siguiente forma: 1.- Si el pago se realizó utilizando tarjetas VISA o
                              MASTERCARD, el pago se realizará el viernes siguiente al corte semanal
                              realizado. Los pagos de las cantidades que correspondan a "EL RES
                              TAURANTE" se realizarán en la forma antes descrita mediante transfe
                              rencia electrónica a la cuenta bancaria que para tal efecto haya promo
                              cionado "EL RESTAURANTE"; en caso de existir alguna duda o inconfor
                              midad con relación al monto a recibir por la prestación de sus servicios,
                              podrá solicitar aclaración de pago mediante correo electrónico, misma
                              que será atendida en un plazo no mayor a cinco días hábiles contados
                              a partir del día siguiente de su recepción. "EL RESTAURANTE" tendrá
                              acceso dentro de la aplicación móvil YAPPAPP al estado de cuenta que
                              refleje las cantidades que recibirá. QUINTA. MODIFICACIÓN DE PRE
                              CIOS, CATÁLOGO Y OTROS.- Las partes acuerdan que para el caso de
                              que sea necesario realizar modificaciones: A) los precios de los platillos
                              que se ofrezcan mediante la aplicación móvil YAPPAPP. B) la variedad
                              de platillos o sus ingredientes. C) "EL RESTAURANTE" tendrá la obliga
                              ción de notificar a la "OPERADORA DE APLICACIONES AMÉRICA"
                              SEXTA. OBLIGACIONES DE "EL RESTAURANTE".-son obligaciones y res
                              ponsabilidades de "EL RESTAURANTE" las siguientes: Ofrecer alimentos
                              frescos y recién elaborados, de la misma calidad que ofrece en su esta
                              blecimiento, pero en la modalidad COMIDA PARA LLEVAR. Respetar y
                              seguir al pie de la letra a las indicaciones que sean realizadas por el
                              usuario de la aplicación móvil YAPPAPP. En cuanto a los ingredientes
                              que deban contener cada platillo, y en su caso término de cocción. Ela
                              borar el platillo apegándose al tiempo de preparación que proporcionó
                              en el cuestionario a que se refiere el inciso seis de la cláusula segunda
                              anterior. Los platillos elaborados deberán ser colocados en el recipiente,
                              bolsa o empaque adecuados para su debida transportación y podrá in
                              cluir elementos de publicidad con los logotipos de su empresa como
                              son bolsas, empaques, stickers, folletos, volantes y/o tarjetas. Permitir
                              que "OPERADORA DE APLICACIONES AMÉRICA" promocione la aplica
                              ción móvil YAPPAPP, con previo acuerdo de las partes se podrán reali
                              zar activaciones en sus establecimientos, colocación de pendones e im
                              presos de lonas, folletería...
                              Deberán informar a "OPERADORA DE APLICACIONES AMÉRICA" con
                              respecto a cualquier motivo que le impida continuar con la prestación
                              del servicio. Lo tendrá que realizar via correo electrónico con acuse de
                              recibido en un plazo no mayor de quince días naturales, contados a
                              partir de que tenga conocimiento de la imposibilidad para continuar
                              con la prestación del servicio. para que "OPERADORA DE APLICACIO
                              NES AMÉRICA" pueda dar de baja a "EL RESTAURANTE" de la aplica
                              ción móvil YAPPAPP. En caso de no hacerlo en la forma y plazos antes
                              señalados, se hará acreedor a una pena convencional que asciende a la
                              cantidad de $1,500 pesos (un mil quinientos pesos), misma que deberá
                              pagar a "OPERADORA DE APLICACIONES AMÉRICA" en su domicilio a
                              más tardar cinco dias hábiles posteriores a su incumplimiento. VIGEN
                              CIA. El contrato tendrá una vigencia de seis meses, contados a partir
                              de la firma del presente. Acuerdan las partes que el presente contrato
                              podrá renovarse por periodos iguales de forma automática, siempre y
                              cuando ambas partes se encuentren al corriente en el cumplimiento de
                              las obligaciones pactadas en el presente instrumento. En el caso de que
                              "OPERADORA DE APLICACIONES AMÉRICA" no informe a "EL RESTAU
                              RANTE" de la terminación de este contrato. "OPERADORA DE APLICA
                              CIONES AMÉRICA" estará obligado a pagar a "EL RESTAURANTE"
                              aquellos servicios que efectivamente se hayan prestado. RELACIÓN LA
                              BORAL. Cada una de las partes manifiesta que son el único patrón y
                              único responsable de todas las relaciones de trabajo de las personas a
                              cargo de cada una de las partes y que realizan las actividades por su
                              cuenta y riesgo y por ello se comprometen a liberar de responsabilidad
                              y sacar en paz y a salvo a los intereses de su contraparte en caso de
                              que se vea involucrada.
                              JURISDICCIÓN. Para todo lo relativo a la interpretación, cumplimiento o
                              resolución del presente contrato, las partes se someten a la competen
                              cia de los Tribunales de la República Mexicana , renunciando expresa
                              mente al fueron que pudiera corresponderles por razón de su domicilio
                              presente o futuro, o cualquier otra circunstancia.
                              Habiendo leído LAS PARTES el presente contrato y enteradas de su
                              contenido y alcance legal, lo firman por duplicado en QUÉRETARO, el
                              <script>
                                  var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                                  var f = new Date();
                                  document.write(f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
                              </script>.
                          </font>
                      </p>
                      <div style="float: right;">
                          <input type="checkbox" id="chkTyC" class="form-check-input">
                          Acepto términos y condiciones
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
          </div>

      </div>
  </div>
  