var cont = 1;
var contImg = 1;
var cont2 = 1;
var cont3 = 1;
var cont4 = 1;
var cont5 = 1;
var cont6 = 1;
var cont7 = 1;
var contTInputFile = 0;
var checkEmail = true;
var food = new Array();

//Tabla dinámica
function addTable() {
  var fila = '<tr class="selected" id="RowTable' + cont2 + '"><td name="tdInput"><input type="text" class="form-control" placeholder="Ingresa la descripción" id="foodInput' + (cont5++) + '" required></td><td id="tdImg' + cont + '"><div name="divImg"><input type="text" value=1 hidden id="cont'+cont+'"><div class="input-file input-file--reverse"><input type="text" disabled style="background: transparent;" id="lblFoodFile' + (cont7) + '" class="input-file__field"><label class="btn btn-primary">Seleccionar Archivo<input type="file" class="form-control" accept="image/*" hidden id="FoodInputFile' + (cont6++) + '" onchange="return fileValidationMenu(' + (cont3++) + ')"></label></div></div></td><td><input type="button" class="btn btn-outline-success" onClick="addImg(' + cont + ')" value="Agregar imagen"><br><br><input type="button" class="btn btn-outline-danger" onClick="deleteFormTable(' + cont2 + ')" value="Eliminar"></td></tr>'
  $('#tableFood').append(fila);
  cont++;
  cont2++;
  contImg = contImg + 3;
}
//Agregar input type file dimánica
function addImg(id) {
  var fila = '<div name="divImg"><div class="input-file input-file--reverse"><input type="text" disabled style="background: transparent;" id="lblFoodFileAdd' + (cont4) + '" class="input-file__field"><label class="btn btn-primary">Seleccionar Archivo<input type="file" class="form-control" accept="image/*" hidden id="FoodInputFileAdd' + (cont4) + '" onchange="return fileValidationMenuAdd(' + (cont4) + ')"></label></div></div>'
  var c = parseInt($('#cont'+id).val()) + 1;
  $('#cont'+id).val(c);
  $('#tdImg' + id).append(fila);
  cont4++
}
//Borrar de la tabla
function deleteFormTable(id) {
  var r = confirm("¿Seguro que deseas eliminar este platillo?");
  if (r) {
    $('#RowTable' + id).remove();
    deletArrayFood(id);
  }
}
//Sólo permite números
function onlyNumber(e) {
  var keynum = window.event ? window.event.keyCode : e.which;
  if ((keynum == 8) || (keynum == 48))
    return true;

  return /\d/.test(String.fromCharCode(keynum));
}
//Valida email
function validateEmail(txt) {
  var txt = document.getElementById("formControlEmailInput").value;
  var regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

  if (!regex.test(txt)) {
    document.getElementById("resp").innerHTML = "Correo invalido";
    checkEmail = false;
  } else {
    document.getElementById("resp").innerHTML = "";
    checkEmail = true;
  }

}
//Manda información 
function sendInfo() {
  //alert("funciona");
  var nameC = document.getElementById("formControlName").value;
  //var imgC = document.getElementById("formControlImgInput").value;
  var addressC = document.getElementById("formControlAddressInput").value;
  var RFC = document.getElementById("formControlRFCInput").value;
  var RZ = document.getElementById("formControlBusinessNameInput").value;
  var email = document.getElementById("formControlEmailInput").value;
  var openTime = document.getElementById("formControlOpenTimeInput").value;
  var closeTime = document.getElementById("formControlCloseTimeInput").value;
  var ordernum = document.getElementById("formControlOrderNumeberInput").value;
  var contactNum = document.getElementById("formControlContactNumeberInput").value;
  var emergencyNum = document.getElementById("formControlEmergencyNumeberInput").value;
  var bill = document.getElementsByName("factura");
  var select = false;
  var billData = "";
  for (var i = 0; i < bill.length; i++) {
    if (bill[i].checked) {
      select = true;
      billData = bill[i].value;
      break;
    }
  }
  var position = document.getElementById("formControlPositionInput").value;
  var bank = document.getElementById("formControlBankInput").value;
  var accountNumber = document.getElementById("formControlAccountNumberInput").value;
  var clabe = document.getElementById("formControlClabeInput").value;
  var owber = document.getElementById("formOwnerInput").value;
  //Sacar los datos de la tabla
  var table = document.getElementById("tableFood");
  var tdTable = table.getElementsByTagName("td");

  var tdInput = document.getElementsByName("tdInput");

  var fileInputImg = document.getElementById('formControlImgInput');
  var fileInputLi = document.getElementById('formControlLicenseInput');
  var fileInputINE = document.getElementById('formControlINEInput');
  var divImg = document.getElementsByName('divImg');

  if (fileInputImg.value == '' || fileInputLi.value == '' || fileInputINE.value == '' || nameC == '' || addressC == '' || RFC == '' || RZ == '' || email == '' || openTime == '' || closeTime == '' || ordernum == '' || contactNum == '' || emergencyNum == '' || !select || position == '' || bank == '' || accountNumber == '' || clabe == '' || owber == '' || checkEmail == false) {
    alert("Debes llenar todos los campos")
    if (fileInputImg.value == '') {
      document.getElementById('lblLogo').style.borderColor = "#FC0303"
    } else {
      document.getElementById('lblLogo').style.borderColor = "#626262"
    }
    if (fileInputLi.value == '') {
      document.getElementById('lblLicence').style.borderColor = "#FC0303"
    } else {
      document.getElementById('lblLicence').style.borderColor = "#626262"
    }
    if (fileInputINE.value == '') {
      document.getElementById('lblINE').style.borderColor = "#FC0303"
    } else {
      document.getElementById('formControlINEInput').style.borderColor = "#626262"
    }
    if (nameC == '') {
      document.getElementById("formControlName").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlName").style.borderColor = "#626262";
    }
    if (addressC == '') {
      document.getElementById("formControlAddressInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlAddressInput").style.borderColor = "#626262";
    }
    if (RFC == '') {
      document.getElementById("formControlRFCInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlRFCInput").style.borderColor = "#626262";
    }
    if (RZ == '') {
      document.getElementById("formControlBusinessNameInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlBusinessNameInput").style.borderColor = "#626262";
    }
    if (email == '') {
      document.getElementById("formControlEmailInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlEmailInput").style.borderColor = "#626262";
    }
    if (openTime == '') {
      document.getElementById("formControlOpenTimeInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlOpenTimeInput").style.borderColor = "#626262";
    }
    if (closeTime == '') {
      document.getElementById("formControlCloseTimeInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlCloseTimeInput").style.borderColor = "#626262";
    }
    if (ordernum == '') {
      document.getElementById("formControlOrderNumeberInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlOrderNumeberInput").style.borderColor = "#626262";
    }
    if (contactNum == '') {
      document.getElementById("formControlContactNumeberInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlContactNumeberInput").style.borderColor = "#626262";
    }
    if (emergencyNum == '') {
      document.getElementById("formControlEmergencyNumeberInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlEmergencyNumeberInput").style.borderColor = "#626262";
    }
    if (position == '') {
      document.getElementById("formControlPositionInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlPositionInput").style.borderColor = "#626262";
    }
    if (bank == '') {
      document.getElementById("formControlBankInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlBankInput").style.borderColor = "#626262";
    }
    if (accountNumber == '') {
      document.getElementById("formControlAccountNumberInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlAccountNumberInput").style.borderColor = "#626262";
    }
    if (clabe == '') {
      document.getElementById("formControlClabeInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formControlClabeInput").style.borderColor = "#626262";
    }
    if (owber == '') {
      document.getElementById("formOwnerInput").style.borderColor = "#FC0303";
    } else {
      document.getElementById("formOwnerInput").style.borderColor = "#626262";
    } if (cont6 > 0) {
      inputValidation();
    }if (cont4 > 0){
      //fileValidationAdd();
    }if (cont7 > 0){
      fileValidation(); 
    }

  }
  else if (tdInput.length < 5 || tdInput == null) {
    alert("Debe colocar minimo 5 platillos distintos");
  } else {
    var img = false;
    var img = [];
    var checkInput = false;
    for (var i = 1; i <= tdInput.length; i++) {
      if ($("#foodInput" + i).val() == '') {
        checkInput = true
        break;
      } else {
        food.push($("#foodInput" + i).val());
      }
    }
    if (checkInput) {
      alert("Debes llenar todos los campos");
    } 
    }
    for(var i=1; i<=cont; i++){
      if(parseInt($('#cont'+i).val())<3){
        alert("Revisa tus archivos, debes incluir en cada platillo mínimo 3 imágenes");
        img = false
        break;
    }else{
      for(var i=1 ; i<=cont; i++){
        if($("#lblFoodFileAdd"+cont).val() == ''){
          alert("Debes agregar todas las imagenes");
          fileValidation();
          fileValidationAdd();
          img = false
          break;
        }
      }
      for(var i=1 ; i<=cont; i++){
        if($("#lblFoodFile"+cont).val() == ''){
          alert("Debes agregar todas las imagenes");
          fileValidation();
          fileValidationAdd();
          img = false
          break;
        }
      }
      if(img){
        alert("Correcto");
        break;
      }
    }
  }
}


function deleteArrayFood(id) {
  food.splice(pos, id);
}

function fileValidationImg() {
  var fileInputImg = document.getElementById('formControlImgInput');
  var filePath = fileInputImg.value;
  var namePath = filePath.slice(12);
  var allowedExtensions = /(.jpg|.jpeg|.png|.gif)$/i;
  $("#lblLogo").val(namePath);
  if (!allowedExtensions.exec(filePath)) {
    alert('Por favor, sólo subir archivos de este tipo .jpeg/.jpg/.png/.gif.');
    fileInputImg.value = '';
    document.getElementById('formControlImgInput').style.borderColor = "#FC0303"
    $("#lblLogo").val("");
    return false;
  }
}
function fileValidationPDF() {
  var fileInputLi = document.getElementById('formControlLicenseInput');
  var filePathLi = fileInputLi.value;
  var namePath = filePathLi.slice(12);
  $("#lblLicence").val(namePath)
  var allowedExtensions = /(.pdf|.PDF)$/i;
  if (!allowedExtensions.exec(filePathLi)) {
    alert('Por favor, sólo subir archivos PDF');
    $("#lblLicence").val("")
    return false;
  }
}

function fileValidationPDFINE() {
  var fileInputINE = document.getElementById('formControlINEInput');
  var filePathINE = fileInputINE.value;
  var namePath = filePathINE.slice(12);
  $("#lblINE").val(namePath);
  var allowedExtensions = /(.pdf|.PDF)$/i;
  if (!allowedExtensions.exec(filePathINE)) {
    alert('Por favor, sólo subir archivos PDF');
    $("#lblINE").val("");
    return false;
  }
}

function fileValidationMenu(id) {
  var fileInput = document.getElementById('FoodInputFile' + id);
  var filePath = fileInput.value;
  var namePath = filePath.slice(12);
  $("#lblFoodFile" + id).val(namePath);
  var allowedExtensions = /(.jpg|.jpeg|.png|.gif)$/i;
  if (!allowedExtensions.exec(filePath)) {
    alert('Por favor, sólo subir archivos de este tipo .jpeg/.jpg/.png/.gif.');
    fileInput.value = '';
    $("#lblFoodFile" + id).val("");
    return false;
  }
}

function fileValidationMenuAdd(id) {
  var fileInput = document.getElementById('FoodInputFileAdd' + id);
  var filePath = fileInput.value;
  var namePath = filePath.slice(12);
  $("#lblFoodFileAdd" + id).val(namePath);
  var allowedExtensions = /(.jpg|.jpeg|.png|.gif)$/i;
  if (!allowedExtensions.exec(filePath)) {
    alert('Por favor, sólo subir archivos de este tipo .jpeg/.jpg/.png/.gif.');
    fileInput.value = '';
    $("#lblFoodFileAdd" + id).val("");
    return false;
  }
}

function fileValidation() {
  
  for (var i = 1; i < cont6; i++) {
    if ($("#FoodInputFile" + i).val() == '') {
      document.getElementById("lblFoodFile" + i).style.borderColor = "#FC0303";
    } else {
      document.getElementById("lblFoodFile" + i).style.borderColor = "#626262";
    }
  }
}

function fileValidationAdd() {
  
  for (var i = 1; i < cont; i++) {
    if ($("#FoodInputFileAdd" + i).val() == '') {
      document.getElementById("lblFoodFileAdd" + i).style.borderColor = "#FC0303";
    } else {
      document.getElementById("lblFoodFileAdd" + i).style.borderColor = "#626262";
    }
  }
}

function inputValidation() {
  for (var n = 1; n <= cont5; n++) {
    if ($("#foodInput" + n).val() == '') {
      document.getElementById("foodInput" + n).style.borderColor = "#FC0303";
    } else {


      document.getElementById("foodInput" + n).style.borderColor = "#626262"

      
    }
  }
}

